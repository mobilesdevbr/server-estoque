// Projeto: Estoque
//
// Licença: <https://gitlab.com/mobilesdevbr/server-estoque/blob/master/LICENSE>
//
// @author Gleison M.v <https://gitlab.com/GleisonMV>
//
import { ObjectId } from "bson";
import Model from "../face/Model";
import { Db } from "mongodb";

class RoleModel implements Model {
  main: ObjectId;
  admin: ObjectId;
  database: Db;

  async init(database: Db): Promise<any> {
    this.database = database;
    this.main = await this.getID("main");
    this.admin = await this.getID("admin");
  }

  /**
   * Retorna o id da função
   *
   * @param name Nome da função
   */
  async getID(name: string): Promise<null | ObjectId> {
    const result = await this.database
      .collection("role")
      .findOne({ name }, { projection: { _id: true } });
    return result == null ? null : result._id;
  }

  /**
   * Cria a função
   *
   * @param name Nome da função
   * @param permission Lista de permissão
   */
  async create(name: string, permission: string[]): Promise<any> {
    return this.database.collection("role").insertOne({ name, permission });
  }
}

//
// Exporta a classe
//
export default RoleModel;
