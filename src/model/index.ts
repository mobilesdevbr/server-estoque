import { MongoClient } from "mongodb";

import Account from "./account";
import Role from "./role";

const Models: { role?: Role; account?: Account } = {};

/**
 * Carrega o banco de dados e modelos
 */
export async function load(): Promise<any> {
  //
  // Inicializa o mongodb
  //
  const mongo = new MongoClient(process.env.DB_URL, {
    useNewUrlParser: true,
    useUnifiedTopology: true
  });
  const client = await mongo.connect();
  const database = client.db(process.env.DB_NAME);
  //
  // Inicializa os Modelos
  //
  Models.role = new Role();
  await Models.role.init(database);

  Models.account = new Account();
  await Models.account.init(database);
}

export default Models;
