// Projeto: Estoque
//
// Licença: <https://gitlab.com/mobilesdevbr/server-estoque/blob/master/LICENSE>
//
// @author Gleison M.v <https://gitlab.com/GleisonMV>
//
import * as pwd from "../lib/password";
import Model from "../face/Model";
import { User } from "../types";
import { Db } from "mongodb";
import Models from ".";

class UserModel implements Model {
  setup: boolean;
  database: Db;

  /**
   * Inicializa o modelo
   *
   * @param database Instância do bd
   */
  async init(database: Db): Promise<any> {
    this.database = database;
    const size = await this.database.collection("account").countDocuments();
    this.setup = size === 0;
  }

  /**
   * Busca o usuário com base no email e senha
   *
   * @param email Email do usuário
   * @param password Senha do usuário
   */
  async getUser(email: string, password: string): Promise<User | null> {
    const account = await this.database
      .collection("account")
      .findOne({ email });
    if (account != null) {
      const equals = await pwd.compare(password, account.password);
      return equals ? account : null;
    }
    return null;
  }

  /**
   * Cria o usuário
   *
   * @param name Nome do usuário
   * @param email Email do usuário
   * @param _password Senha do usuário
   */
  async create(name: string, email: string, _password: string): Promise<any> {
    const password = await pwd.hash(_password);
    const role =
      this.setup && !(this.setup = false)
        ? Models.role.admin
        : Models.role.main;
    return this.database
      .collection("account")
      .insertOne({ name, email, password, role });
  }
}

export default UserModel;
