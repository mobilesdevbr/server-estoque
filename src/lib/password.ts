// Projeto: Estoque
//
// Licença: <https://gitlab.com/mobilesdevbr/server-estoque/blob/master/LICENSE>
//
// @author Gleison M.v <https://gitlab.com/GleisonMV>
//

import crypto from "crypto";

export type Config = {
  KeyLen: number;
  SaltSize: number;
  Iterations: number;
  Digest: string;
};

export const config: Config = {
  KeyLen: 22,
  SaltSize: 16,
  Iterations: 15000,
  Digest: "sha256"
};

/**
 * Cria o hash com base na senha
 *
 * @param {string} password Senha
 * @return {Promisse<String>} Hash Gerado
 */
export function hash(password: string): Promise<string> {
  return new Promise<string>(function(resolve, reject): void {
    crypto.randomBytes(config.SaltSize, function(err, salt) {
      /* istanbul ignore if  */
      if (err) reject(err);
      else {
        const Hex = salt.toString("hex");
        crypto.pbkdf2(
          password,
          Hex,
          config.Iterations,
          config.KeyLen,
          config.Digest,
          function(err: Error, hash: Buffer): void {
            /* istanbul ignore if  */
            if (err) reject(err);
            else resolve([Hex, hash.toString("hex")].join("."));
          }
        );
      }
    });
  });
}
/**
 * Compara a senha e o hash
 *
 * @param {string} password Senha
 * @param {string} hash Hash gerado
 */
export function compare(password: string, hash: string): Promise<boolean> {
  return new Promise<boolean>(function(resolve, reject): void {
    const input = hash.split(".");
    crypto.pbkdf2(
      password,
      input[0],
      config.Iterations,
      config.KeyLen,
      config.Digest,
      function(err: Error, hash: Buffer): void {
        /* istanbul ignore if  */
        if (err) reject(err);
        resolve(hash.toString("hex") === input[1]);
      }
    );
  });
}
