// Projeto: Estoque
//
// Licença: <https://gitlab.com/mobilesdevbr/server-estoque/blob/master/LICENSE>
//
// @author Gleison M.v <https://gitlab.com/GleisonMV>
//
import fp from "fastify-plugin";
import { FastifyReply } from "fastify";
import { ServerResponse } from "http";

import validator from "validator";
import jwt from "jsonwebtoken";
import model from "../model/";

/**
 * Autentificação do usuário
 *
 * @param email Email do usuário
 * @param password Senha do usuário (Sem hash)
 * @param reply FastifyReply
 */
async function handleAuth(
  email: string,
  password: string,
  reply: FastifyReply<ServerResponse>
): Promise<any> {
  if (!validator.isEmail(email)) {
    reply.status(400);
    return {
      success: false,
      message: "A Requisição está inválida!"
    };
  }
  const user = await model.account.getUser(email, password);
  if (user == null) {
    return {
      success: false,
      message: "O Usuário não existe ou dados estão incorretos!"
    };
  }
  try {
    const token = await jwt.sign(
      {
        id: user._id,
        role: user.role
      },
      process.env.SECRET_JWT,
      {
        expiresIn: "1h"
      }
    );
    return {
      success: true,
      token
    };
  } catch (e) {
    reply.status(500);
    return {
      success: false,
      message: "Erro interno, tente novamente mais tarde!"
    };
  }
}

export default fp(function(fastify, opts, next) {
  //
  // Decora o plugin com a função de autentificar
  //
  fastify.decorate("handleAuth", handleAuth);
  fastify.addHook("onRoute", function(options) {
    if (options.auth != undefined) {
      if (options.auth.isAuth != undefined) {
      } else if (options.auth.notAuth != undefined) {
      }
      if (options.auth.permission != undefined) {
      }
    }
  });

  next();
});
