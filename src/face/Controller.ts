import { Server } from "../types";

/**
 * Controlador
 */
abstract class Controller {
  http: Server;

  /**
   * Inicializa
   *
   * @param controller Controllador
   * @param http Instância do fastify
   */
  constructor(http: Server) {
    this.http = http;
  }
}

export default Controller;
