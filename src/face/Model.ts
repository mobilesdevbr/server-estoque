import { Db } from "mongodb";

/**
 * Interface do modelo
 */
interface Model {
  /**
   * Inicializa os recursos necessários
   *
   * @param db Database
   */
  init(database: Db): Promise<any>;
}

export default Model;
