import fastify = require("fastify");

/**
 * Interface do conjunto de rotas
 */
abstract class Route<T> {
  controller: T;
  http: fastify.FastifyInstance;

  /**
   * Inicializa
   *
   * @param controller Controllador
   * @param http Instância do fastify
   */
  constructor(controller: T, http: fastify.FastifyInstance) {
    this.controller = controller;
    this.http = http;
  }
}

export default Route;
