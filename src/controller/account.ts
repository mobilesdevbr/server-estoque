// Projeto: Estoque
//
// Licença: <https://gitlab.com/mobilesdevbr/server-estoque/blob/master/LICENSE>
//
// @author Gleison M.v <https://gitlab.com/GleisonMV>
//
import model from "../model/";
import Controller from "../face/Controller";
import { Req, Reply, Server } from "../types";

let self: AccountController;

class AccountController extends Controller {
  constructor(http: Server) {
    super(http);
    self = this;
  }

  /**
   * Rota do login
   *
   * @param request FastifyRequest
   * @param reply  FastifyReply
   */
  async login(request: Req, reply: Reply): Promise<any> {
    const { email, password } = request.body;
    return await self.http.handleAuth(email, password, reply);
  }

  /**
   * Rota do registro
   *
   * @param request FastifyRequest
   * @param reply  FastifyReply
   */
  async register(request: Req, reply: Reply): Promise<any> {
    const { name, email, password } = request.body;
    try {
      const result = await model.account.create(name, email, password);
      if (result == null) {
        reply.status(500);
        return {
          success: false,
          message: "Erro ao cadastrar!"
        };
      }
      return {
        success: true,
        message: "Cadastrado com sucesso!"
      };
    } catch (e) {
      console.log(e);
      reply.status(500);
      return {
        success: false,
        message: "Erro ao cadastrar!"
      };
    }
  }
}

export default AccountController;
