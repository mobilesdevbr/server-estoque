// Projeto: Estoque
//
// Licença: <https://gitlab.com/mobilesdevbr/server-estoque/blob/master/LICENSE>
//
// @author Gleison M.v <https://gitlab.com/GleisonMV>
//
import fastify from "fastify";
import fastifyHelmet from "fastify-helmet";
import authPlugin from "../core/auth";
import AccountRoute from "./account";

/**
 * Carrega o servidor
 */
export async function load(): Promise<any> {
  //
  // Inicializa o fastify
  //
  const fastifyInstance = fastify({ logger: true });

  //
  // Configura o plugin para proteção de Cabeçalho
  //
  fastifyInstance.register(fastifyHelmet, {
    hidePoweredBy: {
      setTo: "PHP 7.3"
    }
  });

  //
  // Configura o plugin de autentificação
  //
  fastifyInstance.register(authPlugin).after(function() {
    //
    // Inicializa a rota
    //
    new AccountRoute(fastifyInstance);
  });

  //
  // Inicia o servidor
  //
  fastifyInstance.listen(parseInt(process.env.PORT), process.env.HOST);
}
