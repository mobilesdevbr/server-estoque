// Projeto: Estoque
//
// Licença: <https://gitlab.com/mobilesdevbr/server-estoque/blob/master/LICENSE>
//
// @author Gleison M.v <https://gitlab.com/GleisonMV>
//
import Route from "../face/Route";
import AccountController from "../controller/account";
import { Server } from "../types";

class AccountRoute extends Route<AccountController> {
  constructor(http: Server) {
    super(new AccountController(http), http);
    http.route({
      method: "POST",
      url: "/api/auth",
      auth: {
        notAuth: true
      },
      handler: this.controller.login,
      schema: {
        body: {
          type: "object",
          properties: {
            email: { type: "string", maxLength: 255 },
            password: { type: "string" }
          },
          required: ["email", "password"]
        }
      }
    });
    http.route({
      method: "POST",
      url: "/api/register",
      auth: {
        notAuth: true
      },
      handler: this.controller.register,
      schema: {
        body: {
          type: "object",
          properties: {
            name: { type: "string", maxLength: 100 },
            email: { type: "string", maxLength: 255 },
            password: { type: "string", maxLength: 20 }
          },
          required: ["name", "email", "password"]
        }
      }
    });
  }
}

export default AccountRoute;
