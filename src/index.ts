// Projeto: Estoque
//
// Licença: <https://gitlab.com/mobilesdevbr/server-estoque/blob/master/LICENSE>
//
// @author Gleison M.v <https://gitlab.com/GleisonMV>
//

import dotenv from "dotenv";

import { load as LoadModel } from "./model";
import { load as LoadServer } from "./route";

//
// Carrega a configuração do .env
//
dotenv.config();

//
// Carrega os banco de dados e modelos
//
LoadModel();

//
// Carrega o servidor e rotas/controllers
//
LoadServer();
