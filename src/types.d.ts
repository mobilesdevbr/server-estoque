// Projeto: Estoque
//
// Licença: <https://gitlab.com/mobilesdevbr/server-estoque/blob/master/LICENSE>
//
// @author Gleison M.v <https://gitlab.com/GleisonMV>
//
import "fastify";
import { ServerResponse } from "http";
import { ObjectId } from "bson";
import { FastifyReply, FastifyRequest, FastifyInstance } from "fastify";

declare type Server = FastifyInstance;
declare type Req = FastifyRequest;
declare type Reply = FastifyReply<ServerResponse>;

//
// Declara o usuário
//
declare interface User {
  _id: ObjectId;
  email: string;
  password: string;
  role: ObjectId;
}

//
// Extendendo o módulo do fastify
//
declare module "fastify" {
  interface FastifyInstance {
    //
    // Declara a função handleAuth no fastify
    //
    handleAuth: (
      email: string,
      password: string,
      reply: FastifyReply<ServerResponse>
    ) => void;
  }

  //
  // Extende a função do fastify na rota para aceitar o auth
  //
  interface RouteShorthandOptions {
    auth: {
      isAuth?: boolean;
      notAuth?: boolean;
      permission?: string;
    };
  }
}
