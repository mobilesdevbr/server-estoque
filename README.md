# Estoque - Server

## Iniciando desenvolvimento

- 1° Para iniciar o desenvolvimento faça o clone desse projeto 
```
git clone https://gitlab.com/mobilesdevbr/server-estoque.git
``` 
- 2° Instale as dependências do projeto
```
npm install
```
- 3° Instale as dependências global Necessárias:  
```
npm install typescript eslint mocha ts-node migrate-mongo --global
```