// Projeto: Estoque
//
// Licença: <https://gitlab.com/mobilesdevbr/server-estoque/blob/master/LICENSE>
//
// @author Gleison M.v <https://gitlab.com/GleisonMV>
//

'use strict'

module.exports = {
  async down(db) {
    await db.collection('role').drop()
  },
  async up(db) {
    try {
      const role = await db.createCollection('role', {
        validator: {
          '$jsonSchema': {
            bsonType: 'object',
            properties: {
              'name': { bsonType: 'string', maxLength: 100 },
              'permission': { 
                bsonType: 'array', 
                items: {
                  bsonType: 'string'
                }
              }
            },
            required: ['name', 'permission']
          }
        }
      })
      await role.createIndex({ "name": 1 }, { unique: true })
      await role.insertOne({
        name: "admin",
        permission: [
          "user.insert",
          "user.delete",
          "user.edit",
          "role.list",
          "role.create",
          "role.edit"
        ]
      })
      await role.insertOne({
        name: "main",
        permission: []
      })
    } catch (err) {
      console.log(err)
    }
  }
};