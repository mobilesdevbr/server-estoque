// Projeto: Estoque
//
// Licença: <https://gitlab.com/mobilesdevbr/server-estoque/blob/master/LICENSE>
//
// @author Gleison M.v <https://gitlab.com/GleisonMV>
//

'use strict'

module.exports = {
  async down(db) {
    await db.collection('account').drop()
  },
  async up(db) {
    try {
      const account = await db.createCollection('account', {
        validator: {
          '$jsonSchema': {
            bsonType: 'object',
            properties: {
              'name': { bsonType: 'string', maxLength: 100 },
              'email': { bsonType: 'string', maxLength: 255 },
              'password': { bsonType: 'string', maxLength: 77 },
              'role': { bsonType: 'objectId' }
            },
            required: ['name', 'email', 'password', 'role']
          }
        }
      })
      account.createIndex({ "email": 1 }, { unique: true })
    } catch (err) {
      console.log(err)
    }
  }
};