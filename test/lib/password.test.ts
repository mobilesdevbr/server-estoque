// Projeto: Estoque
//
// Licença: <https://gitlab.com/mobilesdevbr/server-estoque/blob/master/LICENSE>
//
// @author Gleison M.v <https://gitlab.com/GleisonMV>
//
import { hash, compare } from "../../src/lib/password";
import { notStrictEqual, strictEqual } from "assert";

describe("password", function() {
  it("#hash", function() {
    hash("admin123").then(hash => {
      notStrictEqual(hash, null);
    });
  });
  it("#verify", function() {
    hash("admin123").then(hash => {
      notStrictEqual(hash, null);
      compare("admin123", hash).then(result => {
        strictEqual(result, true);
      });
      compare("admin", hash).then(result => {
        strictEqual(result, false);
      });
    });
  });
});
